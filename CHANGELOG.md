# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [2.0.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/compare/v1.0.0...v2.0.0) (2025-03-03)


### ⚠ BREAKING CHANGES

* change rate limit headers

### Features

* add health and service info endpoints ([2e82f5c](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/commit/2e82f5cd604f55fcc1f363d95ad459cd3e20075e))
* change rate limit headers ([e962287](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/commit/e962287dffdba37d9b7e0a5e112ce1e19aff8915))


### Bug Fixes

* remove Retry-After header from UnavailableForLegalReasons response ([569a0ff](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/commit/569a0ff338bccea964a61093730d4a380ddcd19c))

## [1.0.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/compare/v0.1.3...v1.0.0) (2024-12-13)


### Features

* add deregistrationDate to GetPersonRegistrationStatusResponse ([d19f015](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/commit/d19f0159ef7c66c1a1b8234dc9a403c9b64d2373))


### Bug Fixes

* increase max length to 64 for date-time properties. Previous max length of 20 characters prematurely truncated the date-time value. ([b2ba34b](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/commit/b2ba34be38a40c6cc686a331830b7e6831e66983))

## [0.1.3](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/specs/compare/v0.1.2...v0.1.3) (2024-04-29)
